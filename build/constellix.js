const crypto = require('crypto')
const fetch = require('node-fetch')
const {createWriteStream} = require('fs')

async function constellix (
  endpoint,
  api = 'https://api.dns.constellix.com',
  version = 'v1'
) {
  const apiKey = process.env['CONSTELLIX_API_KEY']
  const secretKey = process.env['CONSTELLIX_SECRET_KEY']
  const requestDate = String(Date.now())
  const hmac = crypto.createHmac('sha1', secretKey)
  hmac.update(requestDate)
  const securityToken = `${apiKey}:${hmac.digest('base64')}:${requestDate}`

  const url = `${api}/${version}/${endpoint}`
  const headers = {
    'content-type': 'application/json',
    'x-cns-security-token': securityToken
  }
  const response = fetch(url, {headers})
  return response
}

async function main () {
  const file = createWriteStream('geo-proximities.json')
  const response = await constellix('geoProximities/')
  console.log(`Status: ${response.status}`)
  if (response.ok) {
    // console.log(await response.json())
    response.body.pipe(file)
  } else {
    console.error(await response.text())
  }
}

main()
