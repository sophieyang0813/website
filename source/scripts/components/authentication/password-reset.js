import TemplateElement from '../TemplateElement.js'
import { Client } from '../../auth0-api.js'

class PasswordReset extends TemplateElement {
  constructor () {
    super()
    this.template = '/scripts/components/authentication/password-reset.html'
  }

  connectedCallback () {
    super.connectedCallback()
    this.shadowRoot.addEventListener('submit', this._onSubmit)
  }

  disconnectedCallback () {
    this.shadowRoot.removeEventListener('submit', this._onSubmit)
  }

  async _onSubmit (event) {
    event.preventDefault()

    this.querySelector('#progress').removeAttribute('hidden')
    this.querySelector('#problem').setAttribute('hidden', true)

    const email = event.target.querySelector('#email').value

    const auth0 = new Client()

    try {
      await auth0.resetPassword(email)
    } catch (error) {
      this.querySelector('#problem output').textContent = error
      this.querySelector('#progress').setAttribute('hidden', true)
      this.querySelector('#problem').removeAttribute('hidden')
      return
    }

    window.history.pushState(null, '', '/password-reset-sent')
    window.dispatchEvent(new PopStateEvent('popstate'))
  }
}

window.customElements.define('app-password-reset', PasswordReset)
export default PasswordReset
