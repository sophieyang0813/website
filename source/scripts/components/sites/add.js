import TemplateElement from '../TemplateElement.js'
import { Client } from '../../auth0-api.js'

class SitesAdd extends TemplateElement {
  constructor () {
    super()
    this.template = '/scripts/components/sites/add.html'
  }

  async connectedCallback () {
    await super.connectedCallback()
    const root = this.shadowRoot

    const emojis = [
      '👨‍💻', '👨🏻‍💻', '👨🏼‍💻', '👨🏽‍💻', '👨🏾‍💻', '👨🏿‍💻',
      '👩‍💻', '👩🏻‍💻', '👩🏼‍💻', '👩🏽‍💻', '👩🏾‍💻', '👩🏿‍💻'
    ]
    const random = emojis[Math.floor(Math.random() * emojis.length)]
    root.querySelector('header > p').textContent = random

    const profile = await new Client().profile()
    const username = profile.nickname || profile.name
    root.querySelector('#username')
      .textContent = ` ${username}`
  }
}

window.customElements.define('app-sites-add', SitesAdd)
export default SitesAdd
