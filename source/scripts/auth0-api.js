const clientId = 'da4xy2FDuQ4XTvHv1LCMvtRgtSrwtpMv'
const domain = 'http2.au.auth0.com'
const database = 'Username-Password-Authentication'
const audience = 'https://commons.host/'
const scope = ['openid', 'deploy'].join(' ')

async function inspectError (response) {
  let message
  try {
    const data = await response.json()
    message = data.error_description || data.description || data.error
    if (!message) throw Error
  } catch (error) {
    message = `${response.statusText} ${await response.text()}`
  }
  return new Error(message)
}

export class Client {
  constructor () {
    this.clientId = clientId
    this.domain = domain
    this.database = database
    this.audience = audience
    this.scope = scope
  }

  async request (method, endpoint, headers, body) {
    const url = `https://${this.domain}${endpoint}`
    const options = {
      body: JSON.stringify(body),
      headers: Object.assign({ 'content-type': 'application/json' }, headers),
      method
    }
    const response = await fetch(url, options)
    if (response.ok) {
      const contentType = response.headers.get('content-type') || ''
      if (contentType.startsWith('application/json')) {
        return response.json()
      } else {
        return response.text()
      }
    } else {
      throw await inspectError(response)
    }
  }

  async POST (endpoint, body) {
    return this.request('POST', endpoint, undefined, body)
  }

  async GET (endpoint) {
    const accessToken = localStorage.getItem('app.accessToken')
    const headers = {}
    if (accessToken !== null) {
      headers.authorization = `Bearer ${accessToken}`
    }
    return this.request('GET', endpoint, headers)
  }

  async getTokenByPassword (username, password, scope = this.scope) {
    return this.POST('/oauth/token', {
      connection: this.database,
      client_id: this.clientId,
      grant_type: 'password',
      audience: this.audience,
      scope,
      username,
      password
    })
  }

  async signup (email, password, username) {
    return this.POST('/dbconnections/signup', {
      client_id: this.clientId,
      connection: this.database,
      email,
      password,
      username
    })
  }

  async resetPassword (email) {
    return this.POST('/dbconnections/change_password', {
      client_id: this.clientId,
      connection: this.database,
      email
    })
  }

  async profile () {
    return this.GET('/userinfo')
  }
}
